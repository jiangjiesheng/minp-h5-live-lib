# 小程序和H5环境直播+nginx、ffmpeg推流 



#### 项目介绍
小程序-H5-直播

2018年0703


1、视频直播SDK 开源GitHub
 https://github.com/fork-project/SmarterStreaming
 
2、video.js (自定义UI,很多插件 弹幕 快捷键 点播直播都支持) https://github.com/videojs/video.js

   hls.js （点播直播都行）https://github.com/video-dev/hls.js
	
   flv.js （http协议 ） https://github.com/Bilibili/flv.js
   
   //rtmp 实际用的比较少
   //另外我这里的hls测试失败
   
          微信小程序
 		相关代码见胜行天下网小程序 暂定 放松模块 目前仅支持 flv, rtmp 格式（暂时linux中rtmp成功 其他失败）
		https://developers.weixin.qq.com/miniprogram/dev/component/live-player.html
		https://developers.weixin.qq.com/miniprogram/dev/api/api-live-player.html



> 作者：[江节胜](https://www.baidu.com/s?wd=%E6%B1%9F%E8%8A%82%E8%83%9C%20%E8%83%9C%E8%A1%8C%E5%A4%A9%E4%B8%8B%E7%BD%91)

> 微信：**767000122  (欢迎添加好友)**

> Q Q ：596957738

> 个人网站：tech.jiangjiesheng.cn 

> 联系邮箱：dev@jiangjiesheng.cn